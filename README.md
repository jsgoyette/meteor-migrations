# Package migrations

Means to easily define migration scripts for upgrading your packages. At this
point the utility does not gather the version automatically from within Meteor,
but instead relies on the package author to manually set the version by
registering the package.

The package makes available the object `Migrations`, which contains the
necessary methods to register and add new migration tasks.

## Register a package for version tracking

For the package to be tracked, it must first be registered and given a version.
Here, the version should be the current designated version of the package (same
as the version in the `package.js` file).

```
// register with the package name and current version
Migrations.registerPackage('my:package', '1.2.5');
```

If it's the first time that the package is getting registered, you may want to
set the version that the package should hypothetically be migrating from. That
can be done by passing the version as the third argument. If no default version
is provided and it's the first time the package is registered, it will assume
that it should migrate from 0.0.1 to the specified current version. Pass a
default version, then, to prevent existing migrations from running on the first
time.

```
// register using a default version
Migrations.registerPackage('my:package', '1.2.5', '1.2.4');
```

## Add a migration task

Once registered, migration tasks are added using the `add` method. The one thing
to note is that on startup the task will be called if the previous version of
the package is **_less than or equal_** to the "from" version of the migration,
and the current version of the package is **_greater than or equal_** to the
"to" version of the migration.

```
Migrations.add('my:package', '1.2.4', '1.2.5', function() {

  // set the owner field to author
  People.find().forEach(function(doc) {
    People.direct.update({ _id: doc._id }, {
      $set: {
        owner: doc.author
      }
    })
  });

});
```
