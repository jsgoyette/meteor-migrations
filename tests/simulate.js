MigrationVersions.remove({});

// console.log(MigrationVersions.find().fetch())

Migrations.registerPackage('Testing123', '1.3.4');
Migrations.registerPackage('Testing456', '2.0.0', '1.11.0');
Migrations.registerPackage('Testing789', '2.0.0');

Migrations.add('Testing123', '1.2.3', '1.2.4', function() { return console.log('Updating 1.2.3 to 1.2.4'); });
Migrations.add('Testing123', '1.1.2', '1.2.0', function() { return console.log('Updating 1.1.2 to 1.2.0'); });
Migrations.add('Testing123', '1.2.0', '1.2.3', function() { return console.log('Updating 1.2.0 to 1.2.3'); });
Migrations.add('Testing123', '1.2.0', '1.2.1', function() {
  // throw new Error('somethings messed up');
  return console.log('Updating 1.2.0 to 1.2.1');
});

Migrations.add('Testing456', '1.10.2', '1.10.3', function() { return console.log('Updating 1.10.2 to 1.10.3'); });
