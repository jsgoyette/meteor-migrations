
Tinytest.add('Package Registration - register package', function (test) {
  Migrations._clear();

  Migrations.registerPackage('TestPackage', '1.4.2');
  test.equal(true, true);
});

Tinytest.add('Package Registration - enforce package version', function (test) {
  Migrations._clear();

  let success = false;
  try {
    Migrations.registerPackage('TestPackage');
  }
  catch(e) {
    success = true;
  }
  test.equal(success, true);
});

Tinytest.add('Package Registration - enforce previous package version if set', function (test) {
  Migrations._clear();

  let success = false;
  try {
    Migrations.registerPackage('TestPackage', '1.3.4', 'abc');
  }
  catch(e) {
    success = true;
  }
  test.equal(success, true);

  // try again with correct version format
  Migrations.registerPackage('TestPackage2', '1.3.4', '1.3.3');
});

Tinytest.add('Package Registration - enforce previous package is less than specified version', function (test) {
  Migrations._clear();

  let success = false;
  try {
    Migrations.registerPackage('TestPackage', '1.3.4', '1.3.5');
  }
  catch(e) {
    success = true;
  }
  test.equal(success, true);
});
