Migrations = {};

RegisteredPackages = {};
RegisteredMigrations = {};

/**
 * Version schema:
 * @var name [String]
 * @var version [String]
 */
MigrationVersions = new Mongo.Collection('migrationVersions');

/**
 * Migrations.clear
 * for testing
 */
Migrations._clear = function() {
  RegisteredPackages = {};
}

/**
 * Migrations.registerPackage
 * add package to tracking
 * @param name [String] - name of package
 * @param version [String] - semver version the package should be considered at now
 * @param previous [String] - default semver version the package should have
 *  been considered at before if this is the first time for it to be registered
 */
Migrations.registerPackage = function(name, version, previous) {

  check(name, String);

  if (RegisteredPackages[name]) {
    throw new Error('Migrations RegisterPackage - Package already registered');
  }

  // parse the version string
  version = PackageVersion.parse(version);

  if (previous) {

    previous = PackageVersion.parse(previous);

    // make sure previous is less than version
    if (! PackageVersion.lessThan(previous, version)) {
      throw new Error('Migrations RegisterPackage - '
        + 'Previous version should be less than specified version');
    }
  }
  else {
    previous = {};
  }

  RegisteredPackages[name] = { name, version, previous };

};

/**
 * Migrations.add
 * add a new migration task
 * @param name [String] name of package
 * @param from [String] semver version to be migrated from
 * @param to [String] semver version to be migrated to
 * @param task [Function] function to be called
 */
Migrations.add = function(name, from, to, task) {

  check(name, String);
  check(task, Function);

  // validate by parsing
  parsedFrom = PackageVersion.parse(from);
  parsedTo = PackageVersion.parse(to);

  if (! PackageVersion.lessThan(from, to)) {
    throw new Error('Migration Add - '
      + 'From version should be less than to version');
  }

  if (! RegisteredMigrations[name]) {
    RegisteredMigrations[name] = [];
  }

  const entry = { name, from, to, task };

  RegisteredMigrations[name].push(entry);
};


/**
 * Migrations.getCurrentVersion
 * get the version that the package is currently at
 * if not found in database, resort to default
 * @param name [String] name of package
 * @param defaultVersion [String] version to fall back to if not stored
 */
Migrations.getCurrentVersion = function(name, defaultVersion) {

  check(name, String);

  const current = MigrationVersions.findOne({ name });

  if (current) {
    return current.version;
  }

  if (!defaultVersion) {
    return '0.0.1';
  }

  // parse version to validate it
  parsedVersion = PackageVersion.parse(defaultVersion);

  return defaultVersion;
};

/**
 * Migrations.updateVersion
 * update package version in the database
 * generally should not be called manually
 * @param name [String] name of package
 * @param version [String] version to be set to
 */
Migrations.updateVersion = function(name, version) {

  check(name, String);

  // parse version to validate it
  parsedVersion = PackageVersion.parse(version);

  MigrationVersions.upsert({ name }, { $set: { version } });
};
