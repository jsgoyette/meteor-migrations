Package.describe({
  name: 'jsgoyette:migrations',
  version: '0.2.3',
  summary: 'Define package-level migrations',
  git: 'https://jsgoyette@bitbucket.org/jsgoyette/meteor-migrations.git',
  documentation: 'README.md'
});

Package.onUse(function(api) {

  api.versionsFrom('1.2.0.1');

  api.use([
    'check',
    'mongo',
    'underscore',
    'ecmascript',
    'package-version-parser',
    'nooitaf:colors@1.1.2_1',
  ], 'server');

  api.addFiles([
    'migrations.js',
    'startup.js',
  ], 'server');

  api.export([
    'Migrations',
    'MigrationVersions',
  ], 'server');
});

Package.onTest(function(api) {

  api.use([
    'tinytest',
    'underscore',
    'ecmascript',
    'jsgoyette:migrations',
  ]);

  api.addFiles([
    'tests/registration.js',
    'tests/simulate.js',
  ], 'server');
});
