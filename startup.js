// version comparison utility
function lte(version1, version2) {
  return version1 == version2
    || PackageVersion.lessThan(version1, version2);
};

function runMigration({ name, from, to, task }) {

  console.log('Running migration...'.cyan);
  console.log(`\t${name.underline} ${from} - ${to}`);

  Meteor.defer(() => {
    task.call();
  });
};

function fetchMigrationTasks(name, current, target) {

  const migrations = RegisteredMigrations[name];

  if (!migrations) return [];

  // filter the migrations where from >= current and to <= target
  // then sort migrations primarily by from and secondarily by to
  return _(migrations).chain()
    .filter(({ from, to }) => {
      return lte(current, from) && lte(to, target);
    })
    .sortBy(migration => migration.to)
    .sortBy(migration => migration.from)
    .value();

};

function processPackageMigrations({ name, version, previous }) {

  if (process.env['SKIP_MIGRATIONS']) return;

  const targetVersion = version.version;
  const defaultVersion = previous.version;
  const currentVersion = Migrations.getCurrentVersion(name, defaultVersion);

  // do not run if current version equals target
  if (currentVersion == targetVersion) return;

  console.log(
    'Running migrations for:'.cyan,
    `${name} ${currentVersion} - ${targetVersion}`
  );

  // to track progress in case of error
  let newFrom = currentVersion;

  // get the migration tasks within the bounds of the
  // current and target versions
  let args = [name, currentVersion, targetVersion];
  const migrations = fetchMigrationTasks(...args);

  if (migrations.length) {
    console.log((`${migrations.length} migration tasks found...`).cyan);
  }

  try {

    migrations.forEach(migration => {
      newFrom = migration.from;
      runMigration(migration);
    });

  }
  catch(e) {

    // update the version so that it does not redo the tasks already done
    // FIXME: though if there are several migration tasks with the same
    // from version, they could be executed again on the next run
    Migrations.updateVersion(name, newFrom);

    console.log('Migrations Error: '.red);
    console.log((e.stack || e.message).yellow)
    console.log('Migrations ended in error'.red);

    return;
  }

  // if successful update version to target
  Migrations.updateVersion(name, targetVersion);
  console.log('Migrations complete'.green);

};


// process each registered package on startup
Meteor.startup(function() {

  _.each(RegisteredPackages, processPackageMigrations);

});
